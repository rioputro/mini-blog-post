# MiniBlogPost

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.4. This project is about getting posts, users and comments. Build with [Angular 12](https://blog.angular.io/angular-v12-is-now-available-32ed51fbfd49), generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.2.4, and [TailwindCSS](https://tailwindcss.com/)

API sources : [JSON Placeholder](https://jsonplaceholder.typicode.com/)

You can visit the [DEMO](https://mini-blog-post-4p3fvuvtd-rioputroo.vercel.app/) here.
Username and password are username as in [JSON Placeholder](https://jsonplaceholder.typicode.com/) users API.
(e.g Bret)

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.
