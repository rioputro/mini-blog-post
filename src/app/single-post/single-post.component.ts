import {Component, Input, OnInit} from '@angular/core';
import {IPosts} from "@models/posts";
import {PostsService} from "../services/posts.service";

@Component({
  selector: 'app-single-post',
  templateUrl: './single-post.component.html',
  styleUrls: ['./single-post.component.scss']
})
export class SinglePostComponent implements OnInit {

  @Input() post: IPosts;
  @Input() username: string;

  postId = 0;
  totalComments = 0;

  constructor(private postsService: PostsService) {
  }

  ngOnInit(): void {
    this.setPostId();
    this.getPostsComments();
  }

  setPostId(): void {
    if (this.post) {
      this.postId = this.post.id;
    }
  }

  getPostsComments(): void {
    this.postsService.getPostsComments(this.postId).subscribe((comments) => {
      if (comments) {
        this.totalComments = comments.length;
      }
    })
  }
}
