import {Component, OnInit} from '@angular/core';
import {AuthService} from "../services/auth.service";
import {FormBuilder, FormControl, FormGroup, Validators} from "@angular/forms";
import {IUser} from "@models/user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private authService: AuthService,
              private fb: FormBuilder,
              private router: Router) {
  }

  get username(): FormControl {
    return this.loginForm.get('username') as FormControl;
  }

  get password(): FormControl {
    return this.loginForm.get('password') as FormControl;
  }

  ngOnInit(): void {
    this.initForm();
  }

  initForm(): void {
    this.loginForm = this.fb.group({
      username: ['', Validators.required],
      password: ['', Validators.required]
    })
  }

  login(): void {
    if (this.loginForm.valid) {
      this.authService.getUsers().subscribe((users) => {
        if (users) {
          this.validateUsers(users);
        }
      })
    }
  }

  validateUsers(listUsers: IUser[]) {
    if (this.username && this.password) {
      const isUserAvailable = listUsers.filter((user) => user.username === this.username.value && user.username === this.password.value);
      if (isUserAvailable.length > 0) {
        this.setCredentials(listUsers);
      }
    }
  }

  setCredentials(listUsers: IUser[]) {
    if (!localStorage.getItem('creds')) {
      const param = {
        name: listUsers[0].name,
        username: listUsers[0].username,
        id: listUsers[0].id,
        email: listUsers[0].email,
        address: listUsers[0].address.city,
        phone: listUsers[0].phone
      }

      localStorage.setItem('creds', JSON.stringify(param));
      this.router.navigate(['/home']);
    }
  }
}
