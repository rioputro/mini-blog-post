export interface IPostsComments {
  body: string,
  email: string,
  id: number,
  name: string,
  postId: number
}
