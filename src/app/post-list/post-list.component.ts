import {Component, OnInit} from '@angular/core';
import {PostsService} from "../services/posts.service";
import {IPosts} from "@models/posts";

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
})
export class PostListComponent implements OnInit {

  postList: IPosts[];
  post: IPosts;

  username = 'Abit';
  postId = 0;

  constructor(private postsService: PostsService) {
  }

  ngOnInit(): void {
    this.getName();
    this.getPosts();
  }

  getName(): void {
    if (localStorage.getItem('creds')) {
      const creds = JSON.parse(localStorage.getItem('creds'));
      this.username = creds.username;
      this.postId = creds.id;
    }
  }

  getPosts(): void {
    this.postsService.getPosts().subscribe((posts) => {
      if (posts) {
        this.postList = posts.filter((post) => post.userId === this.postId);
      }
    })
  }
}
