import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {IPosts} from "@models/posts";
import {Observable} from "rxjs";
import {environment} from "@env/environment";
import {IPostsComments} from "@models/posts-comments";

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(private http: HttpClient) {
  }

  getPosts(): Observable<IPosts[]> {
    return this.http.get<IPosts[]>(`${environment.BASE_API_URL}posts`, {
      responseType: 'json',
      observe: 'body'
    })
  }

  getPost(id: number): Observable<IPosts> {
    return this.http.get<IPosts>(`${environment.BASE_API_URL}posts/${id}`, {
      responseType: 'json',
      observe: 'body'
    })
  }

  getPostsComments(id: number): Observable<IPostsComments[]> {
    return this.http.get<IPostsComments[]>(`${environment.BASE_API_URL}posts/${id}/comments`, {
      responseType: 'json',
      observe: 'body'
    })
  }


}
