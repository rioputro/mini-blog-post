import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {PostsService} from "../services/posts.service";
import {IPostsComments} from "@models/posts-comments";

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss']
})
export class PostDetailComponent implements OnInit {

  postComments: IPostsComments[];

  postId = 0;
  postCommentsCount = 0;

  postTitle = '';
  postAuthor = '';
  postBody = '';

  isCommentsActive = false;

  constructor(private route: ActivatedRoute,
              private postsService: PostsService) {
  }

  ngOnInit(): void {
    this.getParams();

    this.getPosts();
    this.getAuthor();
    this.getPostsComments();
  }

  getAuthor(): void {
    if (localStorage.getItem('creds')) {
      const creds = JSON.parse(localStorage.getItem('creds'));
      this.postAuthor = creds.username;
    }
  }

  getParams(): void {
    this.route.params.subscribe(params => {
      this.postId = params['id'];
    })
  }

  getPosts(): void {
    if (this.postId !== 0) {
      this.postsService.getPost(this.postId).subscribe((post) => {
        if (post) {
          this.postTitle = post.title;
          this.postBody = post.body;
        }
      })
    }
  }

  getPostsComments(): void {
    if (this.postId !== 0) {
      this.postsService.getPostsComments(this.postId).subscribe((comments) => {
        if (comments) {
          this.postCommentsCount = comments.length;
          this.postComments = comments;
        }
      })
    }
  }

  showComments(): void {
    this.isCommentsActive = !this.isCommentsActive;
  }
}
