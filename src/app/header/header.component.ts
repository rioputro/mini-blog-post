import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  username = 'Abit';

  isDetailPopup = false;

  constructor() {
  }

  ngOnInit(): void {
    this.getName();
  }

  getName(): void {
    if (localStorage.getItem('creds')) {
      const creds = JSON.parse(localStorage.getItem('creds'));
      this.username = creds.username;
    }
  }

  popDetail(): void {
    this.isDetailPopup = !this.isDetailPopup;
  }
}
