import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  username = '';
  email = '';
  address = '';
  phone = '';

  constructor() {
  }

  ngOnInit(): void {
    this.getName();
  }

  getName(): void {
    if (localStorage.getItem('creds')) {
      const creds = JSON.parse(localStorage.getItem('creds'));
      this.username = creds.username;
      this.email = creds.email;
      this.address = creds.address;
      this.phone = creds.phone;
    }
  }
}
