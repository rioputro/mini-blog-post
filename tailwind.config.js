require('dotenv').config();
const enablePurge = process.env.ENABLE_PURGE || false;

module.exports = {
  prefix: '',
  purge: {
    enabled: enablePurge,
    content: [
      './src/**/*.{html,ts}',
    ]
  },
  darkMode: 'class', // or 'media' or 'class'
  theme: {
    extend: {},
    container: {
      center: true
    }
  },
  variants: {
    extend: {},
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
